﻿using UnityEngine;
using System.Collections;

public class csDestroyEffect : MonoBehaviour
{
  void Start()
  {
    StartCoroutine(WaitAndDestroy());
  }

  IEnumerator WaitAndDestroy()
  {
    yield return new WaitForSeconds(5f);
    Destroy(gameObject);
  }
}
