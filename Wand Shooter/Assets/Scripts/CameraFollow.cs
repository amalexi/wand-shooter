using UnityEngine;

public class CameraFollow : MonoBehaviour
{
  public Transform target;

  public float smoothSpeed = 0.125f;
  public Vector3 offset;

  void LateUpdate()
  {
    Vector3 desiredPosition = target.position + offset;

    if (System.Math.Abs(desiredPosition.x) > 2.5)
    {
      desiredPosition.x = desiredPosition.x > 0 ? 2.5f : -2.5f;
    }

    Vector3 smothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    transform.position = smothedPosition;
  }
}
