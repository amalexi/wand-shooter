using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundTypeEnum
{
    PlayerHurt,
    PlayerDeath,
    EnemyHurt,
    EnemyDeath,
    DoorOpen,
    DoorClose,
    LevelUp,
    ShardPickUp,
    Checkpoint
}
