using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour
{
  [Tooltip("Explosion prefab")]
  public GameObject Explosion;

  private void OnTriggerEnter(Collider collider)
  {
    if (collider.gameObject.tag == "Player")
    {
      var kills_score = GameObject.FindWithTag("GameController").GetComponent<Score>();
      kills_score.IncreaseScore();

      var explosion = Instantiate(Explosion, transform.position, transform.rotation);

      EventManager.Broadcast(Events.PickupEvent);
      SoundManager.Instance.Play(SoundTypeEnum.ShardPickUp, 0.8f);

      Destroy(gameObject);
    }
  }
}
