using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Health))]
public class EnemyController : MonoBehaviour
{
  [Tooltip("The chance the object has to drop")]
  [Range(0, 1)]
  public float DropRate = 1f;

  [Tooltip("The speed in seconds with which the enemy can attack its target")]
  public float BaseAttackSpeed = 2f;

  [Tooltip("Delay after death where the GameObject is destroyed (to allow for animation)")]
  public float DeathDuration = 5f;

  [Tooltip("Enemy speed")]
  public float EnemySpeed = 0.8f;

  [Tooltip("The coefficient that indicates relative power of enemy")]
  public float PowerCoefficient = 1f;
  public float DelayBetweenSpecialActions = -1f;
  public GameObject ShardPrefab;

  public float MinDist = 1f;
  public int level = 1;
  public bool isBoss;
  public bool IsDead;
  public float damage = 1;
  public virtual EnemyTypesEnum EnemyType { get; private set; }

  protected GameObject Player;
  protected PlayerController PlayerController;
  protected float m_LastTimeSpecialActed;
  protected Animator animator;

  private Rigidbody rb;
  EnemyManager m_EnemyManager;
  UnityAction onDamaged;
  float m_LastTimeAttacked = Mathf.NegativeInfinity;
  Health m_Health;
  Health playerHealth;
  GameFlowManager m_GameFlowManager;

  private const float defaultEnemySpeed = 0.8f;

  virtual protected void Start()
  {
    m_EnemyManager = FindObjectOfType<EnemyManager>();

    m_EnemyManager.RegisterEnemy(this);

    animator = GetComponent<Animator>();
    rb = GetComponent<Rigidbody>();
    animator.SetFloat("animSpeed", EnemySpeed);
    animator.keepAnimatorControllerStateOnDisable = false;

    m_Health = GetComponent<Health>();

    // Subscribe to damage & death actions
    m_Health.OnDie += OnDie;
    m_Health.OnDamaged += OnDamaged;

    Player = GameObject.FindWithTag("Player");
    playerHealth = Player.GetComponent<Health>();
    PlayerController = Player.GetComponent<PlayerController>();

    m_GameFlowManager = FindObjectOfType<GameFlowManager>();

    m_LastTimeSpecialActed = Time.time;

    if (isBoss)
    {
      transform.localScale *= 1.5f;
      m_Health.MaxHealth *= 2f;
      m_Health.CurrentHealth = m_Health.MaxHealth;
    }
  }

  void Update()
  {
    if (IsDead || m_GameFlowManager.GameIsEnding)
    {
      animator.SetBool("running", false);
      var rigidbody = gameObject.GetComponent<Rigidbody>();
      rigidbody.isKinematic = true;
      return;
    }

    Vector3 D = Player.transform.position - transform.position;
    Quaternion rot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(D), EnemySpeed * 20 * Time.deltaTime);
    transform.rotation = rot;
    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    Action();
  }

  virtual protected void Action()
  {
    if (m_LastTimeSpecialActed + DelayBetweenSpecialActions < Time.time)
    {
      SpecialAction();
    }

    var distance = Vector3.Distance(transform.position, Player.transform.position);

    if (distance >= MinDist)
    {
      var isRunning = animator.GetBool("running");
      if (!isRunning)
      {
        animator.SetBool("running", true);
      }
    }
    else
    {
      StartCoroutine(TryAttack());
      animator.SetBool("running", false);
    }
  }

  protected virtual void SpecialAction()
  {
    m_LastTimeSpecialActed = Time.time;
  }

  void OnDamaged(float damage, GameObject damageSource)
  {
    animator.SetTrigger("Hitted");
    SoundManager.Instance.Play(SoundTypeEnum.EnemyHurt, 0.8f);
  }

  void OnDie()
  {
    //loot an object
    Instantiate(ShardPrefab, transform.position + new Vector3(0, 0.7f, 0), Quaternion.identity);

    IsDead = true;

    var capsuleCollider = GetComponent<CapsuleCollider>();
    Destroy(capsuleCollider);

    rb.isKinematic = true;

    m_EnemyManager.UnregisterEnemy(this);

    animator.SetTrigger("Death");
    SoundManager.Instance.Play(SoundTypeEnum.EnemyDeath);

    // this will call the OnDestroy function
    Destroy(gameObject, DeathDuration);
  }

  public IEnumerator TryAttack()
  {
    if ((m_LastTimeAttacked + BaseAttackSpeed / EnemySpeed) >= Time.time
      || PlayerController.IsDead
      || (DelayBetweenSpecialActions > 0 && (m_LastTimeSpecialActed + BaseAttackSpeed / (EnemySpeed * 2)) >= Time.time))
    {
      yield break;
    }

    m_LastTimeAttacked = Time.time;
    animator.SetTrigger("Attacking");
    yield return new WaitForSeconds(0.4f / (EnemySpeed / defaultEnemySpeed));
    var distance = Vector3.Distance(transform.position, Player.transform.position);
    if (distance <= MinDist + 0.3)
    {
      playerHealth.TakeDamage(damage, this.gameObject);
    }
  }
}
