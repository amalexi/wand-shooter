using UnityEngine;

public class ShootingEnemy : EnemyController
{
  [SerializeField]
  private GameObject weaponParentSocket;

  [SerializeField]
  private WeaponController defaultWand;

  private WeaponController activeWand;
  private float firstShotDelay;
  private float startTime;

  public override EnemyTypesEnum EnemyType => EnemyTypesEnum.Shooter;

  override protected void Start()
  {
    base.Start();
    activeWand = Instantiate(defaultWand, weaponParentSocket.transform.position + new Vector3(0, 0, 0.1f), new Quaternion());
    activeWand.Owner = gameObject;
    activeWand.transform.parent = weaponParentSocket.transform;
    firstShotDelay = UnityEngine.Random.Range(0f, 2f);
    startTime = Time.time;
  }

  protected override void Action()
  {
    Vector3 D = Player.transform.position - transform.position;
    Quaternion rot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(D), EnemySpeed * 20 * Time.deltaTime);
    transform.rotation = rot;
    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

    Shoot();
  }

  private void Shoot()
  {
    if (PlayerController.IsDead || startTime + firstShotDelay > Time.time)
    {
      return;
    }

    Vector3 shootPoint = Player.transform.position + new Vector3(0, 1, 0);

    var isShot = activeWand.TryShoot(shootPoint);
    if (isShot)
    {
      animator.SetTrigger("Attacking");
    }
  }
}
