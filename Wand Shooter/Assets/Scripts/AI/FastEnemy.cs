using UnityEngine;

public class FastEnemy : EnemyController
{
  public override EnemyTypesEnum EnemyType => EnemyTypesEnum.Sprinter;

  override protected void Start()
  {
    base.Start();
    DelayBetweenSpecialActions = 3f;
  }

  protected override void SpecialAction()
  {
    m_LastTimeSpecialActed = Time.time;
    var xOffset = UnityEngine.Random.Range(0.5f, 1f);
    var zOffset = UnityEngine.Random.Range(0.5f, 1f);

    var distance = Vector3.Distance(transform.position, Player.transform.position);
    if (distance > 2.5)
    {
      transform.position = Player.transform.position + new Vector3(xOffset, 0, zOffset);
    }
  }
}
