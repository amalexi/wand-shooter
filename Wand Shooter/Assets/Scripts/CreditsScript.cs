using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScript : MonoBehaviour
{
  public GameObject creditsUI;

  public void ShowCredits()
  {
    creditsUI.SetActive(true);
  }

  public void CloseCredits()
  {
    creditsUI.SetActive(false);
  }
}
