using UnityEngine;
using UnityEngine.UI;

public class WandDamageUpdater : MonoBehaviour
{
  public void UpdateDamage(float damage)
  {
    gameObject.GetComponent<Text>().text = $"{damage}";
  }
}