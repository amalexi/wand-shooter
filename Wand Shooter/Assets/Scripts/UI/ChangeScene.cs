using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
  public int roomNumber = 1;

  public void LoadScene(string sceneName)
  {
    var savedRoomNumber = GlobalVariables.Get<int>("checkPointRoomNumber");
    GlobalVariables.Set("checkPointRoomNumber", savedRoomNumber != 0 ? savedRoomNumber : roomNumber);

    Time.timeScale = 1;
    PauseMenu.IsGamePaused = false;
    SceneManager.LoadScene(sceneName);
  }
}
