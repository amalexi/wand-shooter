using UnityEngine;
using UnityEngine.UI;

public class CurrentProgressText : MonoBehaviour
{
  void Start()
  {
    GameDataManager.LoadGameData();

    var checkpointRoomNumber = GlobalVariables.Get<int>("checkPointRoomNumber");
    var wandLevel = GlobalVariables.Get<int>("wandLevel");

    if (checkpointRoomNumber == 0)
    {
      gameObject.SetActive(false);
      return;
    }

    var progressText = GetComponent<Text>();
    progressText.text = $"Resume from room # {checkpointRoomNumber}";
  }
}
