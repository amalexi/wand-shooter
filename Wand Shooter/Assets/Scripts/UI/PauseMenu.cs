using UnityEngine;

public class PauseMenu : MonoBehaviour
{
  public static bool IsGamePaused;
  public GameObject pauseMenuUI;

  public void PauseGame()
  {
    Time.timeScale = 0;
    pauseMenuUI.SetActive(true);
    IsGamePaused = true;

    GoogleAdMobController.Instance.RequestBannerAd();
  }

  public void ResumeGame()
  {
    Time.timeScale = 1;
    pauseMenuUI.SetActive(false);
    IsGamePaused = false;

    GoogleAdMobController.Instance.DestroyBannerAd();
  }
}
