using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdater : MonoBehaviour
{
  public void UpdateScore(int score, int levelUpScoreAmount)
  {
    gameObject.GetComponent<Text>().text = $"{score}/{levelUpScoreAmount}";
  }
}