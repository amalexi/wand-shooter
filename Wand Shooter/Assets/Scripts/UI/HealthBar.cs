using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
  public GameObject HeartFull;
  public GameObject HeartHalf;

  private Health playerHealth;
  private List<GameObject> hearts = new List<GameObject>();
  private float heartWidth;

  void Start()
  {
    playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
    heartWidth = ((RectTransform)HeartFull.transform).rect.width;
    SetHealthText((int)playerHealth.MaxHealth);
  }

  public void SetHealthText(int curHealth)
  {
    foreach (var item in hearts)
    {
      Destroy(item);
    }

    if (curHealth <= 0)
    {
      return;
    }

    int i;
    for (i = 0; i < curHealth / 2; i++)
    {
      var heart = Instantiate(HeartFull, new Vector3(i * (heartWidth + heartWidth * 0.2f), 0, 0), Quaternion.identity);
      heart.transform.SetParent(this.transform, false);
      hearts.Add(heart);
    }

    if (curHealth % 2 != 0)
    {
      var heart = Instantiate(HeartHalf, new Vector3(i * (heartWidth + heartWidth * 0.2f), 0, 0), Quaternion.identity);
      heart.transform.SetParent(this.transform, false);
      hearts.Add(heart);
    }
  }
}
