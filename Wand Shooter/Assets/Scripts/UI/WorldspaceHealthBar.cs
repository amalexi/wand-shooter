using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorldspaceHealthBar : MonoBehaviour
{
  [Tooltip("Health component to track")] public Health Health;

  [Tooltip("Image component displaying health left")]
  public Image HealthBarImage;

  [Tooltip("Text component displaying enemy level")]
  public TextMeshPro LevelText;

  [Tooltip("The floating healthbar pivot transform")]
  public Transform HealthBarPivot;

  [Tooltip("Whether the health bar is visible when at full health or not")]
  public bool HideFullHealthBar = true;

  public EnemyController enemy;

  void Update()
  {
    // update health bar value
    HealthBarImage.fillAmount = Health.CurrentHealth / Health.MaxHealth;

    // rotate health bar to face the camera/player
    HealthBarPivot.LookAt(Camera.main.transform.position);

    // hide health bar if needed
    if (HideFullHealthBar)
      HealthBarPivot.gameObject.SetActive(HealthBarImage.fillAmount != 1);

    LevelText.SetText(enemy.level.ToString());
  }
}