using System.Collections.Generic;
using UnityEngine;

public class ProjectileStandard : MonoBehaviour
{
  [Header("Movement")]
  [Tooltip("Speed of the projectile")]
  public float Speed = 10f;

  [Tooltip("Transform representing the root of the projectile (used for accurate collision detection)")]
  public Transform Root;

  [Header("General")]
  [Tooltip("Radius of this projectile's collision detection")]
  public float Radius = 0.01f;

  [Tooltip("LifeTime of the projectile")]
  public float MaxLifeTime = 5f;

  [Tooltip("Layers this projectile can collide with")]
  public LayerMask HittableLayers = -1;

  [Header("Damage")]
  [Tooltip("Damage of the projectile")]
  public float Damage = 10f;

  [Tooltip("Explosion prefab")]
  public GameObject Explosion;

  public GameObject Owner { get; private set; }
  public Vector3 InitialPosition { get; private set; }
  public Vector3 InitialDirection { get; private set; }

  List<Collider> m_IgnoredColliders;
  Vector3 m_Velocity;
  float m_ShootTime;
  Vector3 m_LastRootPosition;

  const QueryTriggerInteraction k_TriggerInteraction = QueryTriggerInteraction.Collide;

  void OnEnable()
  {
    Destroy(gameObject, MaxLifeTime);
  }

  // Update is called once per frame
  void Update()
  {
    // Move
    transform.position += m_Velocity * Time.deltaTime;

    // Hit detection
    RaycastHit closestHit = new RaycastHit();
    closestHit.distance = Mathf.Infinity;
    bool foundHit = false;

    // Sphere cast
    Vector3 displacementSinceLastFrame = transform.position - m_LastRootPosition;
    RaycastHit[] hits = Physics.SphereCastAll(m_LastRootPosition, Radius,
        displacementSinceLastFrame.normalized, displacementSinceLastFrame.magnitude, HittableLayers,
        k_TriggerInteraction);
    foreach (var hit in hits)
    {
      if (IsHitValid(hit) && hit.distance < closestHit.distance)
      {
        foundHit = true;
        closestHit = hit;
      }
    }

    if (foundHit)
    {
      // Handle case of casting while already inside a collider
      if (closestHit.distance <= 0f)
      {
        closestHit.point = Root.position;
        closestHit.normal = -transform.forward;
      }

      OnHit(closestHit.point, closestHit.normal, closestHit.collider);
    }

    m_LastRootPosition = Root.position;
  }

  public void Shoot(WeaponController controller)
  {
    Owner = controller.Owner;
    m_ShootTime = Time.time;
    m_LastRootPosition = transform.position;
    m_Velocity = transform.forward * Speed;

    InitialPosition = transform.position;
    InitialDirection = transform.forward;

    m_IgnoredColliders = new List<Collider>();
    Collider[] ownerColliders = Owner.GetComponentsInChildren<Collider>();
    m_IgnoredColliders.AddRange(ownerColliders);
  }

  bool IsHitValid(RaycastHit hit)
  {
    // ignore hits with an ignore component
    if (hit.collider.GetComponent<IgnoreHitDetection>())
    {
      return false;
    }

    // ignore hits with triggers that don't have a Damageable component
    if (hit.collider.isTrigger && hit.collider.GetComponent<Damageable>() == null)
    {
      return false;
    }

    // ignore hits with specific ignored colliders (self colliders, by default)
    if (m_IgnoredColliders != null && m_IgnoredColliders.Contains(hit.collider))
    {
      return false;
    }

    return true;
  }

  void OnHit(Vector3 point, Vector3 normal, Collider collider)
  {
    // damage
    Damageable damageable = collider.GetComponent<Damageable>();
    if (damageable)
    {
      damageable.InflictDamage(Damage, false, Owner);
    }

    var explosion = Instantiate(Explosion, transform.position, transform.rotation);

    // Self Destruct
    Destroy(this.gameObject);
  }
}
