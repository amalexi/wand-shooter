using System;
using System.Collections;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
  [Header("Information")]
  [Tooltip("The name that will be displayed in the UI for this weapon")]
  public string WeaponName;

  [Tooltip("The image that will be displayed in the UI for this weapon")]
  public Sprite WeaponIcon;

  [Tooltip("Tip of the weapon, where the projectiles are shot")]
  public Transform WeaponMuzzle;

  [Tooltip("The projectile prefab")]
  public ProjectileStandard ProjectilePrefab;

  [Tooltip("Minimum duration between two shots")]
  public float DelayBetweenShots = 1.5f;

  [Tooltip("sound played when shooting")]
  public AudioClip ShootSfx;

  public GameObject Owner { get; set; }
  public int level = 1;

  float m_LastTimeShot = Mathf.NegativeInfinity;

  public bool TryShoot(Vector3 shootPoint)
  {
    if (m_LastTimeShot + DelayBetweenShots < Time.time)
    {
      HandleShoot(shootPoint);

      return true;
    }

    return false;
  }

  public float GetWeaponDamage()
  {
    return (float)Math.Ceiling(ProjectilePrefab.Damage + (ProjectilePrefab.Damage * Math.Pow(level - 1, 2) * 0.3f));
  }

  public void WandLevelUp()
  {
    level++;
    var gameController = GameObject.FindWithTag("GameController");
    var kills_score = gameController.GetComponent<Score>();
    kills_score.SubstractScoreForUpgrade();

    GlobalVariables.Set("wandLevel", level);
    GameDataManager.SaveGameData();
  }

  void HandleShoot(Vector3 shootPoint)
  {
    m_LastTimeShot = Time.time;

    StartCoroutine(ProjectileShoot(shootPoint));

    // play shoot SFX
    if (ShootSfx)
    {
      SoundManager.Instance.Play(ShootSfx);
    }
  }

  IEnumerator ProjectileShoot(Vector3 shootPoint)
  {
    yield return new WaitForSeconds(0.25f);
    ProjectileStandard newProjectile = Instantiate(ProjectilePrefab, WeaponMuzzle.position, Quaternion.LookRotation(shootPoint - WeaponMuzzle.transform.position));
    newProjectile.Damage = GetWeaponDamage();
    newProjectile.Shoot(this);
  }
}