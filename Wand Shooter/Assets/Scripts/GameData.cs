public class GameData
{
  public int WandLevel;
  public int ShardsCount;
  public int CheckpointRoomNumber;

  public bool IsMusicMute;
  public bool IsSoundMute;
}
