using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.UI;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
  private CharacterController controller;
  private Animator animator;
  private PlayerInput playerInput;
  private Vector3 playerVelocity;
  private GraphicRaycaster raycaster;
  private bool groundedPlayer;

  [SerializeField]
  private GameObject joystick;

  [SerializeField]
  private GameObject weaponParentSocket;

  [Tooltip("Player speed")]
  [Range(0, 1)]
  public float MoveSpeed = 0.5f;

  [SerializeField]
  private WeaponController defaultWand;
  public HealthBar healthBar;
  public WeaponController activeWand;
  public Canvas uiCanvas;
  public EventSystem eventSystem;

  public bool IsDead { get; private set; }

  Health m_Health;

  private void Awake()
  {
    EnhancedTouchSupport.Enable();
  }

  void OnEnable()
  {
    Touch.onFingerDown += Touch_onFingerDown;
  }

  void OnDisable()
  {
    Touch.onFingerDown -= Touch_onFingerDown;
  }

  private void Start()
  {
    controller = GetComponent<CharacterController>();
    animator = GetComponent<Animator>();
    playerInput = GetComponent<PlayerInput>();
    raycaster = uiCanvas.GetComponent<GraphicRaycaster>();

    animator.SetFloat("animSpeed", MoveSpeed);

    activeWand = Instantiate(defaultWand, weaponParentSocket.transform.position + new Vector3(0, 0, 0.1f), new Quaternion());
    activeWand.Owner = gameObject;
    activeWand.transform.parent = weaponParentSocket.transform;

    var wandLevel = GlobalVariables.Get<int>("wandLevel");
    if (wandLevel != 0)
    {
      activeWand.level = wandLevel;
    }

    m_Health = GetComponent<Health>();
    m_Health.OnDie += OnDie;
    m_Health.OnDamaged += OnDamaged;

    var wandDamageUpdater = FindObjectOfType<WandDamageUpdater>();
    wandDamageUpdater.UpdateDamage(activeWand.GetWeaponDamage());
  }

  void OnDie()
  {
    IsDead = true;
    EventManager.Broadcast(Events.PlayerDeathEvent);
    animator.SetTrigger("Dying");
    SoundManager.Instance.Play(SoundTypeEnum.PlayerDeath);
  }

  private void Touch_onFingerDown(Finger latestFinger)
  {
    if (PauseMenu.IsGamePaused || IsDead)
    {
      return;
    }

    foreach (Finger finger in Touch.activeFingers)
    {
      if (Touch.activeFingers.Count > 0)
      {
        var currentTouches = Touch.activeTouches.Where(af => af.phase == UnityEngine.InputSystem.TouchPhase.Began);

        foreach (var touch in currentTouches)
        {
          var touchPosition = touch.startScreenPosition;

          var pointerEventData = new PointerEventData(EventSystem.current);
          pointerEventData.position = touchPosition;

          //check if UI touched
          List<RaycastResult> results = new List<RaycastResult>();
          raycaster.Raycast(pointerEventData, results);

          if (results.Any())
          {
            return;
          }

          Vector3 shootPoint = new Vector3();

          Ray ray = Camera.main.ScreenPointToRay(touchPosition);
          Plane plane = new Plane(Vector3.up, transform.position + Vector3.up);
          float distance = 1.5f;
          if (plane.Raycast(ray, out distance))
          {
            shootPoint = ray.GetPoint(distance);
          }

          var isShot = activeWand.TryShoot(shootPoint);
          if (isShot)
          {
            animator.SetTrigger("Attacking");
          }

          break;
        }
      }
    }
  }

  void Update()
  {
    if (PauseMenu.IsGamePaused || IsDead)
    {
      return;
    }

    Vector2 input = playerInput.actions["Move"].ReadValue<Vector2>();
    Vector3 move = new Vector3(input.x, 0, input.y);
    controller.Move(move * Time.deltaTime * MoveSpeed);

    if (move != Vector3.zero && !IsDead)
    {
      gameObject.transform.forward = move;
    }

    animator.SetBool("running", input.x != 0);
  }

  public void OnDamaged(float damage, GameObject damageSource)
  {
    healthBar.SetHealthText((int)m_Health.CurrentHealth);
    animator.SetTrigger("Hitted");
    SoundManager.Instance.Play(SoundTypeEnum.PlayerHurt, 2);
  }
}