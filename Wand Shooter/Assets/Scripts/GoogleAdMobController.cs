using UnityEngine.Events;
using UnityEngine;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;

public class GoogleAdMobController : MonoBehaviour
{
  private BannerView bannerView;
  string bannerAdUnitId = "ca-app-pub-8462551394032637/1301973186";

  private InterstitialAd interstitialAd;
  string interstitialAdUnitId = "ca-app-pub-8462551394032637/3373403062";

  public UnityEvent OnAdLoadedEvent;
  public UnityEvent OnAdFailedToLoadEvent;
  public UnityEvent OnAdOpeningEvent;
  public UnityEvent OnAdClosedEvent;

  public static GoogleAdMobController Instance;

  #region UNITY MONOBEHAVIOR METHODS
  private void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
    }
    else if (Instance != this)
    {
      Destroy(gameObject);
      return;
    }

    DontDestroyOnLoad(gameObject);
  }

  public void Start()
  {
    // Configure TagForChildDirectedTreatment and test device IDs.
    RequestConfiguration requestConfiguration =
        new RequestConfiguration.Builder()
        .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
        .build();
    MobileAds.SetRequestConfiguration(requestConfiguration);

    // Initialize the Google Mobile Ads SDK.
    MobileAds.Initialize(HandleInitCompleteAction);
  }

  private void HandleInitCompleteAction(InitializationStatus initstatus)
  {
    Debug.Log("Initialization complete.");

    // Callbacks from GoogleMobileAds are not guaranteed to be called on
    // the main thread.
    // In this example we use MobileAdsEventExecutor to schedule these calls on
    // the next Update() loop.
    MobileAdsEventExecutor.ExecuteInUpdate(() =>
    {
      Debug.Log("Initialization complete.");
      RequestAndLoadInterstitialAd();
    });
  }

  #endregion

  #region HELPER METHODS

  private AdRequest CreateAdRequest()
  {
    return new AdRequest.Builder()
        .AddKeyword("unity-admob-sample")
        .Build();
  }

  #endregion

  #region BANNER ADS

  public void RequestBannerAd()
  {
    PrintStatus("Requesting Banner ad.");

    // Clean up banner before reusing
    if (bannerView != null)
    {
      bannerView.Destroy();
    }

    // Create a 320x50 banner at top of the screen
    bannerView = new BannerView(bannerAdUnitId, AdSize.Banner, AdPosition.Bottom);

    // Add Event Handlers
    bannerView.OnAdLoaded += (sender, args) =>
    {
      PrintStatus("Banner ad loaded.");
      OnAdLoadedEvent.Invoke();
    };
    bannerView.OnAdFailedToLoad += (sender, args) =>
    {
      PrintStatus("Banner ad failed to load with error: " + args.LoadAdError.GetMessage());
      OnAdFailedToLoadEvent.Invoke();
    };
    bannerView.OnAdOpening += (sender, args) =>
    {
      PrintStatus("Banner ad opening.");
      OnAdOpeningEvent.Invoke();
    };
    bannerView.OnAdClosed += (sender, args) =>
    {
      PrintStatus("Banner ad closed.");
      OnAdClosedEvent.Invoke();
    };
    bannerView.OnPaidEvent += (sender, args) =>
    {
      string msg = string.Format("{0} (currency: {1}, value: {2}",
                                      "Banner ad received a paid event.",
                                      args.AdValue.CurrencyCode,
                                      args.AdValue.Value);
      PrintStatus(msg);
    };

    // Load a banner ad
    bannerView.LoadAd(CreateAdRequest());
  }

  public void DestroyBannerAd()
  {
    if (bannerView != null)
    {
      bannerView.Destroy();
    }
  }

  #endregion

  #region INTERSTITIAL ADS

  public void RequestAndLoadInterstitialAd()
  {
    PrintStatus("Requesting Interstitial ad.");

    // Clean up interstitial before using it
    if (interstitialAd != null)
    {
      interstitialAd.Destroy();
    }

    interstitialAd = new InterstitialAd(interstitialAdUnitId);

    // Add Event Handlers
    interstitialAd.OnAdLoaded += (sender, args) =>
    {
      PrintStatus("Interstitial ad loaded.");
      OnAdLoadedEvent.Invoke();
    };
    interstitialAd.OnAdFailedToLoad += (sender, args) =>
    {
      PrintStatus("Interstitial ad failed to load with error: " + args.LoadAdError.GetMessage());
      OnAdFailedToLoadEvent.Invoke();
    };
    interstitialAd.OnAdOpening += (sender, args) =>
    {
      PrintStatus("Interstitial ad opening.");
      OnAdOpeningEvent.Invoke();
    };
    interstitialAd.OnAdClosed += (sender, args) =>
    {
      PrintStatus("Interstitial ad closed.");
      OnAdClosedEvent.Invoke();
      RequestAndLoadInterstitialAd();
    };
    interstitialAd.OnAdDidRecordImpression += (sender, args) =>
    {
      PrintStatus("Interstitial ad recorded an impression.");
    };
    interstitialAd.OnAdFailedToShow += (sender, args) =>
    {
      PrintStatus("Interstitial ad failed to show.");
    };
    interstitialAd.OnPaidEvent += (sender, args) =>
    {
      string msg = string.Format("{0} (currency: {1}, value: {2}",
                                      "Interstitial ad received a paid event.",
                                      args.AdValue.CurrencyCode,
                                      args.AdValue.Value);
      PrintStatus(msg);
    };

    // Load an interstitial ad
    interstitialAd.LoadAd(CreateAdRequest());
  }

  public void ShowInterstitialAd()
  {
    if (interstitialAd != null && interstitialAd.IsLoaded())
    {
      interstitialAd.Show();
    }
    else
    {
      PrintStatus("Interstitial ad is not ready yet.");
    }
  }

  public void DestroyInterstitialAd()
  {
    if (interstitialAd != null)
    {
      interstitialAd.Destroy();
    }
  }

  #endregion

  #region AD INSPECTOR

  public void OpenAdInspector()
  {
    PrintStatus("Open ad Inspector.");

    MobileAds.OpenAdInspector((error) =>
    {
      if (error != null)
      {
        PrintStatus("ad Inspector failed to open with error: " + error);
      }
      else
      {
        PrintStatus("Ad Inspector opened successfully.");
      }
    });
  }

  #endregion

  #region Utility

  ///<summary>
  /// Log the message and update the status text on the main thread.
  ///<summary>
  private void PrintStatus(string message)
  {
    Debug.Log(message);
    // MobileAdsEventExecutor.ExecuteInUpdate(() =>
    // {
    //   statusText.text = message;
    // });
  }

  #endregion
}