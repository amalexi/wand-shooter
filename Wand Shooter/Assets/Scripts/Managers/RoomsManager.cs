using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomsManager : MonoBehaviour
{
  public List<GameObject> roomPrefabs;

  [SerializeField]
  private Text roomNumberText;

  public int curRoomNumber = 1;

  EnemyManager m_EnemyManager;

  private GameObject[] Rooms;
  RoomScript activeRoom;
  RoomScript nextRoom;

  Vector3 roomSpawnPoint = new Vector3();


  void Awake()
  {
    EventManager.AddListener<RoomCleanedEvent>(OnRoomCleaned);
    EventManager.AddListener<RoomEnterEvent>(OnRoomEnter);
  }

  void Start()
  {
  }

  public void Init()
  {
    Rooms = new GameObject[3];

    var savedRoomNumber = GlobalVariables.Get<int>("checkPointRoomNumber");
    if (savedRoomNumber != 0)
    {
      curRoomNumber = savedRoomNumber;
    }

    roomNumberText.text = curRoomNumber.ToString();

    var roomPrefabIndex = UnityEngine.Random.Range(0, roomPrefabs.Count());
    var roomPrefab = roomPrefabs.ElementAt(roomPrefabIndex);

    var roomBounds = CalculateBounds(roomPrefab);
    var startRoom = Instantiate(roomPrefab, new Vector3(-roomBounds.center.x, 0, -roomBounds.center.z), Quaternion.identity);
    activeRoom = startRoom.GetComponent<RoomScript>();
    Rooms[1] = startRoom;
    var startRoomBounds = CalculateBounds(startRoom);


    float zPos = -startRoomBounds.size.z / 2 - roomBounds.size.z / 2 - roomBounds.center.z;
    roomSpawnPoint.Set(-roomBounds.center.x, 0, zPos);
    var roomBehind = Instantiate(roomPrefab, roomSpawnPoint, Quaternion.identity);
    Rooms[0] = roomBehind;

    zPos = startRoomBounds.size.z / 2 + roomBounds.size.z / 2 - roomBounds.center.z;

    roomSpawnPoint.Set(-roomBounds.center.x, 0, zPos);
    var room = Instantiate(roomPrefab, roomSpawnPoint, Quaternion.identity);
    nextRoom = room.GetComponent<RoomScript>();
    Rooms[2] = room;

    m_EnemyManager = FindObjectOfType<EnemyManager>();

    var enemiesSpawnPoint = activeRoom.GetSpawnPoint();
    m_EnemyManager.SpawnEnemies(enemiesSpawnPoint, curRoomNumber);

  }

  private void OnRoomEnter(RoomEnterEvent evt)
  {
    StartCoroutine(activeRoom.CloseDoor());
    curRoomNumber++;

    var pickableItems = FindObjectsOfType<PickableItem>();
    foreach (var item in pickableItems)
    {
      Destroy(item.gameObject);
    }

    roomNumberText.text = curRoomNumber.ToString();

    var enemiesSpawnPoint = nextRoom.GetSpawnPoint();
    m_EnemyManager.SpawnEnemies(enemiesSpawnPoint, curRoomNumber);
    activeRoom = nextRoom;

    if (Rooms[0] != null)
    {
      Destroy(Rooms[0]);
    }

    Rooms[0] = Rooms[1];
    Rooms[1] = Rooms[2];

    CreateNewRoom();
  }

  private void CreateNewRoom()
  {
    var prevRoomBounds = CalculateBounds(Rooms[1]);

    var roomPrefabIndex = UnityEngine.Random.Range(0, roomPrefabs.Count());
    var roomPrefab = roomPrefabs.ElementAt(roomPrefabIndex);

    //TODO: Chose room prefab
    var roomBounds = CalculateBounds(roomPrefab);

    float zPos = roomSpawnPoint.z + (prevRoomBounds.size.z / 2) + ((roomBounds.size.z / 2) - roomBounds.center.z);
    roomSpawnPoint.Set(-roomBounds.center.x, 0, zPos);

    var room = Instantiate(roomPrefab, roomSpawnPoint, Quaternion.identity);
    nextRoom = room.GetComponent<RoomScript>();
    Rooms[2] = room;
  }

  private void OnRoomCleaned(RoomCleanedEvent evt)
  {
    if (curRoomNumber % 10 == 0)
    {
      //save checkpoint
      GlobalVariables.Set("checkPointRoomNumber", curRoomNumber + 1);
      GameDataManager.SaveGameData();

      var messageManager = FindObjectOfType<MessagesManager>();
      messageManager.ShowMessage("Checkpoint saved!");
      SoundManager.Instance.Play(SoundTypeEnum.Checkpoint);
    }

    nextRoom.ActivateEnterTrigger();
    StartCoroutine(activeRoom.OpenDoor());
  }

  private Bounds CalculateBounds(GameObject gameObject)
  {
    var bounds = new Bounds();
    bounds.size = Vector3.zero; // reset

    Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

    if (renderers.Length == 0)
    {
      return bounds;
    }

    bounds = renderers[0].bounds;
    foreach (var renderer in renderers)
    {
      bounds.Encapsulate(renderer.bounds);
    }

    return bounds;
  }

  void OnDestroy()
  {
    EventManager.RemoveListener<RoomCleanedEvent>(OnRoomCleaned);
    EventManager.RemoveListener<RoomEnterEvent>(OnRoomEnter);
  }
}
