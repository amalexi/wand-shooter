using System.IO;
using UnityEngine;

public static class GameDataManager
{

  public static void SaveGameData()
  {
    var gameData = new GameData();
    gameData.CheckpointRoomNumber = GlobalVariables.Get<int>("checkPointRoomNumber");
    gameData.ShardsCount = GlobalVariables.Get<int>("currentShardsCount");
    gameData.WandLevel = GlobalVariables.Get<int>("wandLevel");
    gameData.IsMusicMute = GlobalVariables.Get<bool>("isMusicMute");
    gameData.IsSoundMute = GlobalVariables.Get<bool>("isSoundMute");

    string filePath = Path.Combine(Application.persistentDataPath, "gameData.json");
    string jsonData = JsonUtility.ToJson(gameData);

    try
    {
      File.WriteAllText(filePath, jsonData);
    }
    catch { }
  }

  public static void LoadGameData()
  {
    var gameData = new GameData();

    string filePath = Path.Combine(Application.persistentDataPath, "gameData.json");

    if (System.IO.File.Exists(filePath))
    {
      try
      {
        string loadData = File.ReadAllText(filePath);
        if (loadData != null)
        {
          gameData = JsonUtility.FromJson<GameData>(loadData);
        }
      }
      catch { }
    }

    GlobalVariables.Set("checkPointRoomNumber", gameData.CheckpointRoomNumber);
    GlobalVariables.Set("wandLevel", gameData.WandLevel);
    GlobalVariables.Set("currentShardsCount", gameData.ShardsCount);
    GlobalVariables.Set("isMusicMute", gameData.IsMusicMute);
    GlobalVariables.Set("isSoundMute", gameData.IsSoundMute);
  }
}
