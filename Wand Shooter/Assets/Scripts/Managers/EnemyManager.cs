using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
  private const int MAX_ENEMY_NUMBER = 5;
  private const float ENEMY_LEVEL_DISPERSION = 0.2f;

  public List<EnemyController> enemiesPrefabs;

  private List<EnemyController> Enemies;
  public int NumberOfEnemiesRemaining => Enemies.Count;

  void Awake()
  {
    Enemies = new List<EnemyController>();

    EventManager.AddListener<EnemyKillEvent>(OnAllEnemiesKilled);
  }

  public void SpawnEnemies(Transform spawnPoint, int roomNumber)
  {
    //TODO: Chose enemy type and level
    var enemyMetaInfos = ChoseEnemyPrefabs(roomNumber);

    foreach (var enemyMetaInfo in enemyMetaInfos)
    {
      var enemyPrefab = enemiesPrefabs.First(p => p.EnemyType == enemyMetaInfo.Item1);
      var enemy = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);
      Init(enemy, enemyMetaInfo.Item2, enemyMetaInfo.Item3);
    }
  }

  private List<Tuple<EnemyTypesEnum, int, bool>> ChoseEnemyPrefabs(int roomNumber)
  {
    var enemies = new List<Tuple<EnemyTypesEnum, int, bool>>();

    float enemiesPower = roomNumber;
    var optimalEnemyLevel = Math.Ceiling(enemiesPower / MAX_ENEMY_NUMBER);
    var minEnemyLevel = (int)Math.Floor(optimalEnemyLevel * (1 - ENEMY_LEVEL_DISPERSION));
    minEnemyLevel = minEnemyLevel == 0 ? 1 : minEnemyLevel;
    var maxEnemyLevel = (int)Math.Ceiling(optimalEnemyLevel * (1 + ENEMY_LEVEL_DISPERSION));
    var weakestEnemy = enemiesPrefabs
      .First(e => e.PowerCoefficient == enemiesPrefabs.Min(e => e.PowerCoefficient));
    float minEnemyPower = weakestEnemy.PowerCoefficient * minEnemyLevel;

    while (enemies.Count() < MAX_ENEMY_NUMBER)
    {
      var a = enemiesPower / minEnemyPower;

      if (a <= 1)
      {
        if (a > 0.5)
        {
          weakestEnemy.level = minEnemyLevel;
          enemies.Add(new Tuple<EnemyTypesEnum, int, bool>(weakestEnemy.EnemyType, minEnemyLevel, false));
        }

        break;
      }

      IEnumerable<EnemyController> availableEnemyTypes = new List<EnemyController>();

      if (enemies.Count > MAX_ENEMY_NUMBER - 3)
      {
        availableEnemyTypes = enemiesPrefabs.Where(e => e.PowerCoefficient * minEnemyLevel > enemiesPower);
      }
      else
      {
        availableEnemyTypes = enemiesPrefabs.Where(e => e.PowerCoefficient * 2 <= (enemiesPower / optimalEnemyLevel));
      }

      if (!availableEnemyTypes.Any())
      {
        availableEnemyTypes = availableEnemyTypes.Concat(new EnemyController[] { weakestEnemy });
      }

      var enemyTypeIndex = UnityEngine.Random.Range(0, availableEnemyTypes.Count());

      var enemy = availableEnemyTypes.ElementAt(enemyTypeIndex);

      int enemyLevel;
      if (enemies.Count > MAX_ENEMY_NUMBER - 3)
      {
        enemyLevel = (int)Math.Ceiling(enemiesPower / enemy.PowerCoefficient);
        enemyLevel = enemyLevel > maxEnemyLevel ? maxEnemyLevel : enemyLevel;
      }
      else
      {
        enemyLevel = UnityEngine.Random.Range(minEnemyLevel, maxEnemyLevel);
      }

      enemies.Add(new Tuple<EnemyTypesEnum, int, bool>(enemy.EnemyType, enemyLevel, false));
      enemiesPower -= enemyLevel * enemy.PowerCoefficient;
    }

    if (roomNumber % 10 == 0)
    {
      var enemyTypeIndex = UnityEngine.Random.Range(0, enemiesPrefabs.Count());

      var enemy = enemiesPrefabs.ElementAt(enemyTypeIndex);
      enemies.Add(new Tuple<EnemyTypesEnum, int, bool>(enemy.EnemyType, maxEnemyLevel, true));
    }

    return enemies;
  }

  private void Init(EnemyController enemy, int level, bool isBoss)
  {
    enemy.level = level;
    var health = enemy.GetComponent<Health>();
    health.MaxHealth += (float)(health.MaxHealth * Math.Pow(enemy.level - 1, 2) * 0.3f);
    enemy.isBoss = isBoss;
  }

  public void RegisterEnemy(EnemyController enemy)
  {
    Enemies.Add(enemy);
  }

  public void UnregisterEnemy(EnemyController enemyKilled)
  {
    int enemiesRemainingNotification = NumberOfEnemiesRemaining - 1;

    EnemyKillEvent evt = Events.EnemyKillEvent;
    evt.Enemy = enemyKilled.gameObject;
    evt.RemainingEnemyCount = enemiesRemainingNotification;
    EventManager.Broadcast(evt);

    // removes the enemy from the list, so that we can keep track of how many are left on the map
    Enemies.Remove(enemyKilled);
  }

  void OnAllEnemiesKilled(EnemyKillEvent evt)
  {
    if (evt.RemainingEnemyCount == 0)
    {
      EventManager.Broadcast(Events.RoomCleanedEvent);
    }
  }

  void OnDestroy()
  {
    EventManager.RemoveListener<EnemyKillEvent>(OnAllEnemiesKilled);
  }
}
