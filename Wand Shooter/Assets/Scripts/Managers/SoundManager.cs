using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
  public AudioSource EffectsSource;

  public List<AudioClip> PlayerHurtSounds;
  public List<AudioClip> PlayerDeathSounds;
  public List<AudioClip> EnemyHurtSounds;
  public List<AudioClip> EnemyDeathSounds;
  public List<AudioClip> DoorOpenSounds;
  public List<AudioClip> DoorCloseSounds;
  public List<AudioClip> LevelUpSounds;
  public List<AudioClip> ShardPickUpSounds;
  public List<AudioClip> CheckpointSounds;


  // Singleton instance.
  public static SoundManager Instance = null;

  // Initialize the singleton instance.
  private void Awake()
  {
    // If there is not already an instance of SoundManager, set it to this.
    if (Instance == null)
    {
      Instance = this;
    }
    //If an instance already exists, destroy whatever this object is to enforce the singleton.
    else if (Instance != this)
    {
      Destroy(gameObject);
    }

    //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
    DontDestroyOnLoad(gameObject);
  }

  // Play a single clip through the sound effects source.
  public void Play(AudioClip clip, float volume = 1)
  {
    var isSoundMute = GlobalVariables.Get<bool>("isSoundMute");
    if (isSoundMute)
    {
      return;
    }

    //EffectsSource.clip = clip;
    EffectsSource.minDistance = 1.0f;
    EffectsSource.PlayOneShot(clip, volume);
  }

  public void Play(SoundTypeEnum soundType, float volume = 1)
  {
    AudioClip clip = null;

    switch (soundType)
    {
      case SoundTypeEnum.PlayerHurt:
        {
          var index = UnityEngine.Random.Range(0, PlayerHurtSounds.Count);
          clip = PlayerHurtSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.PlayerDeath:
        {
          var index = UnityEngine.Random.Range(0, PlayerDeathSounds.Count);
          clip = PlayerDeathSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.EnemyHurt:
        {
          var index = UnityEngine.Random.Range(0, EnemyHurtSounds.Count);
          clip = EnemyHurtSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.EnemyDeath:
        {
          var index = UnityEngine.Random.Range(0, EnemyDeathSounds.Count);
          clip = EnemyDeathSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.DoorOpen:
        {
          var index = UnityEngine.Random.Range(0, DoorOpenSounds.Count);
          clip = DoorOpenSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.DoorClose:
        {
          var index = UnityEngine.Random.Range(0, DoorCloseSounds.Count);
          clip = DoorCloseSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.LevelUp:
        {
          var index = UnityEngine.Random.Range(0, LevelUpSounds.Count);
          clip = LevelUpSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.ShardPickUp:
        {
          var index = UnityEngine.Random.Range(0, ShardPickUpSounds.Count);
          clip = ShardPickUpSounds.ElementAt(index);
          break;
        }
      case SoundTypeEnum.Checkpoint:
        {
          var index = UnityEngine.Random.Range(0, CheckpointSounds.Count);
          clip = CheckpointSounds.ElementAt(index);
          break;
        }
      default:
        Debug.Log($"Error. Sound with type {soundType.ToString()} not found");
        break;
    }

    if (clip == null)
    {
      return;
    }

    Play(clip, volume);
  }

  public void SetMute(bool isMute)
  {
    EffectsSource.volume = isMute ? 0 : 1f;
  }
}
