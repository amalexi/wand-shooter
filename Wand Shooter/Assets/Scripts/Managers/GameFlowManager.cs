using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameFlowManager : MonoBehaviour
{
  [Header("Parameters")]
  [Tooltip("Duration of the fade-to-black at the end of the game")]
  public float EndSceneLoadDelay = 3f;
  public bool showBannerAfterInit;
  public GameObject Explosion;

  public GameObject loseMenuUI;
  public WandDamageUpdater wandDamageUpdater;

  public Button musicButton;
  public Button soundButton;
  public Sprite musicOnImage;
  public Sprite musicOffImage;
  public Sprite soundOnImage;
  public Sprite soundOffImage;

  public bool GameIsEnding { get; private set; }

  GameObject player;
  PlayerController playerController;
  Score killsScore;
  float m_TimeLoadEndGameScene;

  bool isMusicMute;
  bool isSoundMute;

  AudioSource music;

  RoomsManager m_RoomsManager;

  void Awake()
  {
    EventManager.AddListener<PlayerDeathEvent>(OnPlayerDeath);
    EventManager.AddListener<PickupEvent>(OnShardPickUp);
    GameDataManager.LoadGameData();
  }

  void OnEnable()
  {
    SceneManager.sceneLoaded += OnSceneFinishedLoading;
  }

  void Start()
  {
    Application.targetFrameRate = 60;

    music = GameObject.Find("BackgroundMusic").GetComponent<AudioSource>();
    music.ignoreListenerVolume = true;

    player = GameObject.FindWithTag("Player");
    if (player != null)
    {
      player.transform.position = new Vector3(0, 0, -6.5f);
      playerController = player.GetComponent<PlayerController>();
    }

    m_RoomsManager = FindObjectOfType<RoomsManager>();

    m_RoomsManager?.Init();

    killsScore = gameObject.GetComponent<Score>();

    isMusicMute = GlobalVariables.Get<bool>("isMusicMute");
    musicButton.GetComponent<Image>().sprite = isMusicMute ? musicOffImage : musicOnImage;
    music.volume = isMusicMute ? 0 : 0.4f;

    isSoundMute = GlobalVariables.Get<bool>("isSoundMute");
    soundButton.GetComponent<Image>().sprite = isSoundMute ? soundOffImage : soundOnImage;
    if (SoundManager.Instance)
    {
      SoundManager.Instance.SetMute(isSoundMute);
    }

    var reloadsCount = GlobalVariables.Get<int>("reloadsCount");
    if (reloadsCount > 1)
    {
      GoogleAdMobController.Instance.ShowInterstitialAd();
    }

    GlobalVariables.Set("reloadsCount", ++reloadsCount);

    if (showBannerAfterInit)
    {
      GoogleAdMobController.Instance.RequestBannerAd();
    }
  }

  void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
  {
    var path = "Shaders/ShaderVariants";
    var collection = Resources.Load<ShaderVariantCollection>(path);

    if (collection != null)
    {
      collection.WarmUp();
      Resources.UnloadAsset(collection);
    }
  }

  void Update()
  {
    if (GameIsEnding)
    {
      float timeRatio = 1 - (m_TimeLoadEndGameScene - Time.time) / EndSceneLoadDelay;

      // See if it's time to load the end scene (after the delay)
      if (Time.time >= m_TimeLoadEndGameScene && !loseMenuUI.activeSelf)
      {
        loseMenuUI.SetActive(true);
        GoogleAdMobController.Instance.RequestBannerAd();
      }

      return;
    }

    if (PauseMenu.IsGamePaused)
    {
      return;
    }
  }

  private void CheckWandLevelUp()
  {
    if (killsScore.ScoreValue >= killsScore.LevelUpScoreAmount)
    {
      playerController.activeWand.WandLevelUp();

      var explosion = Instantiate(Explosion, player.transform.position, player.transform.rotation);
      explosion.transform.parent = player.transform;

      var messageManager = FindObjectOfType<MessagesManager>();
      messageManager.ShowMessage("Damage increased!");

      SoundManager.Instance.Play(SoundTypeEnum.LevelUp, 1.2f);

      wandDamageUpdater.UpdateDamage(playerController.activeWand.GetWeaponDamage());
    }
  }

  void OnPlayerDeath(PlayerDeathEvent evt) => EndGame();
  void OnShardPickUp(PickupEvent evt) => CheckWandLevelUp();

  void EndGame()
  {
    GameIsEnding = true;
    m_TimeLoadEndGameScene = Time.time + EndSceneLoadDelay;
  }

  void OnDestroy()
  {
    EventManager.RemoveListener<PlayerDeathEvent>(OnPlayerDeath);
    EventManager.RemoveListener<PickupEvent>(OnShardPickUp);
  }

  public void ResetProgress()
  {
    string[] filePaths = Directory.GetFiles(Application.persistentDataPath);
    foreach (string filePath in filePaths)
    {
      File.Delete(filePath);
    }
  }

  public void MuteMusic()
  {
    isMusicMute = !isMusicMute;
    music.volume = isMusicMute ? 0 : 0.4f;
    musicButton.GetComponent<Image>().sprite = isMusicMute ? musicOffImage : musicOnImage;

    GlobalVariables.Set("isMusicMute", isMusicMute);
    GameDataManager.SaveGameData();
  }

  public void MuteSound()
  {
    isSoundMute = !isSoundMute;

    soundButton.GetComponent<Image>().sprite = isSoundMute ? soundOffImage : soundOnImage;

    if (SoundManager.Instance)
    {
      SoundManager.Instance.SetMute(isSoundMute);
    }

    GlobalVariables.Set("isSoundMute", isSoundMute);
    GameDataManager.SaveGameData();
  }
}