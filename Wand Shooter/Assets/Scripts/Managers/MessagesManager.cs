using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MessagesManager : MonoBehaviour
{
  public void ShowMessage(string message)
  {
    var text = gameObject.GetComponent<Text>();
    text.text = message;
    StartCoroutine(WaitAndHide());
  }

  IEnumerator WaitAndHide()
  {
    yield return new WaitForSeconds(2f);
    var text = gameObject.GetComponent<Text>();
    text.text = string.Empty;
  }
}