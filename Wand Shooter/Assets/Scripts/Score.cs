using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
  public ScoreUpdater scoreText;
  public int LevelUpScoreAmount = 20;

  public int ScoreValue { get; private set; }

  // Start is called before the first frame update
  void Start()
  {
    var savedShardCount = GlobalVariables.Get<int>("currentShardsCount");
    ScoreValue = savedShardCount;
    scoreText.UpdateScore(ScoreValue, LevelUpScoreAmount);
  }

  public void IncreaseScore()
  {
    ScoreValue++;
    scoreText.UpdateScore(ScoreValue, LevelUpScoreAmount);

    GlobalVariables.Set("currentShardsCount", ScoreValue);
    GameDataManager.SaveGameData();
  }

  public void SubstractScoreForUpgrade()
  {
    ScoreValue -= LevelUpScoreAmount;
    scoreText.UpdateScore(ScoreValue, LevelUpScoreAmount);

    GlobalVariables.Set("currentShardsCount", ScoreValue);
    GameDataManager.SaveGameData();
  }
}
