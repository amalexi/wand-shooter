using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomScript : MonoBehaviour
{
  private Animator doorAnimator;

  // Start is called before the first frame update
  void Start()
  {
    var door = gameObject.transform.Find("backWall/doorway/doorHolder");
    doorAnimator = door.gameObject.GetComponent<Animator>();
  }

  // Update is called once per frame
  void Update()
  {

  }

  public void ActivateEnterTrigger()
  {
    var enter = gameObject.transform.Find("enter").gameObject;
    enter.SetActive(true);
  }

  public IEnumerator OpenDoor()
  {
    doorAnimator.SetBool("isOpened", true);

    yield return new WaitForSeconds(0.5f);
    SoundManager.Instance.Play(SoundTypeEnum.DoorOpen, 2f);
  }

  public IEnumerator CloseDoor()
  {
    doorAnimator.SetBool("isOpened", false);

    var backWall = gameObject.transform.Find("backWall").gameObject.GetComponent<Collider>();
    backWall.isTrigger = false;

    yield return new WaitForSeconds(0.7f);
    SoundManager.Instance.Play(SoundTypeEnum.DoorClose, 0.8f);
  }

  public Transform GetSpawnPoint()
  {
    var spawnPoint = gameObject.transform.Find("spawnPoint");

    return spawnPoint;
  }
}
