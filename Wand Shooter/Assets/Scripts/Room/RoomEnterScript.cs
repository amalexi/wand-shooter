using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomEnterScript : MonoBehaviour
{
  void OnTriggerEnter(Collider collider)
  {
    PlayerController player = collider.gameObject.GetComponent<PlayerController>();

    if (player)
    {
      gameObject.SetActive(false);
      EventManager.Broadcast(Events.RoomEnterEvent);
    }
  }
}