using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = animator = transform.Find("doorHolder").GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider collider)
    {
        animator.SetBool("isOpened", true);
    }
}
